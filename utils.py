import logging
from threading import Lock


class Log(object):
    def __init__(self):
        self.__logger = logging.getLogger(self.__class__.__name__)
        self.__default_log_lvl = logging.DEBUG

    def log(self, msg=None):
        if msg:
            self.__logger.log(self.__default_log_lvl, msg)
        return self.__logger

    def setLogger(self, logger):
        self.__logger = logger

class Status(Log):
    BLANK, CREATED, STARTING, RUNNING, STOPPED, FINISHED, PASSED, FAILED, ERROR = range(9)
    def __init__(self):
        x = 0
        self.__status_codes = {}
        for status in ['BLANK', 'CREATED', 'STARTING', 'RUNNING', 'STOPPED', 'FINISHED', 'PASSED', 'FAILED', 'ERROR',]:
            self.__status_codes[x]=status
            x = x+1
        super(Status, self).__init__()
        self.__status_lock = Lock()
        self.__status = Status.BLANK

    def setStatus(self, status):
        if status in range(len(self.__status_codes)):
            with self.__status_lock:
                self.__status = status
            self.log().debug('Status set to Status.%s'%self.__status_codes[status])
            return status
        else:
            return False

    def status(self):
        with self.__status_lock:
            return self.__status

    def convert(self, tag=None, number=None):
        if tag:
            statusTag = str(tag.upper())
            self.log('Converting %s to num'%statusTag)
            for code, stat in self.__status_codes.iteritems():
                print(code, stat, statusTag)
                if statusTag == stat:
                    self.log('Converted %s to %i'%(statusTag, code))
                    return code
            return None

        elif number:
            return self.__status_codes.get(number, None)
        
        return None

