import time
import uuid
from utils import Status, Log
import config
from node import NodeManager
from threading import Lock, Thread

class StatusHandler(Status):
    def __init__(self):
        super(StatusHandler, self).__init__()
        self.__statusCallBack = None

    def setStatus(self, status):
        statusCode = super(StatusHandler, self).setStatus(status)
        if statusCode:
            if self.__statusCallBack:
                self.log('statusCallBack set calling...')
                Thread(target=self.__statusCallBack, args=(self,)).start()
                err = None
                if err:
                    self.log().error('StatusHandler FAILED statusCode: %s - Error: %s'%(status, err))
        return statusCode

    def setStatusCallback(self, callback=None):
        if callback:
            self.__statusCallBack = callback
            return True, None
        else:
            return False, 'No callback provided'

class Build(StatusHandler):
    def __init__(self, client_id, git_repo, image='debian:stable', dockerArgs = {}, **kwargs):
        super(Build, self).__init__() 
        self.clientID = client_id
        self.id = uuid.uuid4().hex
        self.buildCreated = time.time()
        self.nodeId = None
        self.git_repo = git_repo
        self.image = image
        self.extra_args = kwargs
        self.dockerArgs = dockerArgs
        self.container = None

    def toDict(self):
        d = {
            'clientId':self.clientID,
            'nodeId': self.nodeId,
            'created': self.buildCreated,
            'gitRepo': self.git_repo,
            'image': self.image,
            'status': self.convert(number=self.status()),
            'dockerArgs': self.dockerArgs,
            }
        if self.container:
            d['containerId'] = self.container.id
        else:
            d['containerId'] = None
        return d        


class BuildManager(Log):
    def __init__(self):
        super(BuildManager, self).__init__()
        
        self.__builds = BuildList()
        self.__nodeManager = NodeManager()
        self.__nodeManager.connect()
        self.__statusHandlers = {
                Status.CREATED:     self.__handlerCREATED,
                Status.STARTING:    self.__dummyHandler,
                Status.RUNNING:     self.__dummyHandler,
                Status.STOPPED:     self.__dummyHandler,
                Status.FINISHED:    self.__handlerFINISHED,
                Status.PASSED:      self.__dummyHandler,
                Status.FAILED:      self.__dummyHandler,
                Status.ERROR:       self.__dummyHandler,
                    }

    def addBuild(self, build):
        self.__builds.addBuild(build)
        build.setStatusCallback(self.__statusUpdateCallback)
        status, err = self.__nodeManager.createBuild(build)
        if err:
            self.log('Error starting Build-%s - Error: %s'%(build.id, err))
        else:
            self.log('Sucessfully Added Build-%s'%build.id)

        return status, err

    def updateBuild(self, build = None, buildId = None, status = None, error = None):
        if not build:
            if buildId:
                build = self.__builds.getBuild(buildId=buildId)
            else:
                self.log().error('No Build or ID provided to updateBuild()')
                return False, 'No Build Provided!'
        else:
            buildId = build.id

        if not status:
            if not error:
                self.log().error('No status or error provided to UpdateBuild(), nothing to upate!')
                return False, 'No Status or Error Provided!'
            else:
                status = Status.ERROR

        self.log('Updating Build-%s to %i'%(buildId, int(status)))
        
        build.setStatus(int(status))
        
        if error:
            build.error = error
        
        return True, None

    def removeBuild(self, build = None, buildId = None):
        if not build:
            if buildId:
                build = self.__builds.getBuild(buildId=buildId)
            else:
                self.log().error('No Build or ID provided to removeBuild()')
                return False, 'No Build Provided'
        status, err = self.__builds.removeBuild(build)
        if err:
            self.log().error('Removing Build-%s FAILED - Error: %s'%(build.id, err))
            return status, err

        return True, None
        
    def listBuilds(self, **kwargs):
        #clientId = None, nodeId = None, status = None, limit=10
        return self.__builds.listBuilds(**kwargs)
        

    def __statusUpdateCallback(self, build):
        status, err = self.__statusHandlers[build.status()](build)
        return status, err

    def __dummyHandler(self, build):
            return True, None

    def __handlerFINISHED(self, build):
        status, err = self.__nodeManager.removeBuild(build)
        if err:
            self.log().error('handlerFINISH FAILED - Build-%s - %s'%(build.id, err))
        else:
            self.removeBuild(build)

        return status, err

    def __handlerCREATED(self, build):
        status, err = self.__nodeManager.startBuild(build)
        #add the build to the buildList again. It only creates missing records if called twice. used To add build.nodeId to internal dict of buildList, kinda hacky but it'll do for now
        self.__builds.addBuild(build)
        if err:
            self.log().error('handlerCREATED FAILED - Build-%s - %s'%(build.id, err))
        return status, err

        

class BuildList(Log):
    def __init__(self):
        super(BuildList, self).__init__()

        self.__buildsByNode = {}
        self.__buildsByClient = {}
        self.__buildsById = {}

    def addBuild(self, build):
        self.__buildsById[build.id] = build
        
        if build.clientID in self.__buildsByClient:
            if build.id not in self.__buildsByClient[build.clientID]:
                self.__buildsByClient[build.clientID].append(build.id)
        else:
            self.__buildsByClient[build.clientID] = [build.id,]

        if build.nodeId:
            if build.nodeId in self.__buildsByNode:
                if build.id not in self.__buildsByNode[build.nodeId]:
                    self.__buildsByNode[build.nodeId].append(build.id)
            else:
                self.__buildsByNode[build.nodeId] = [build.id,]

        self.log('Added Build-%s client: %s - node: %s'%(build.id, build.clientID, build.nodeId))

    def getBuild(self, buildId = None, nodeId = None, clientId = None, status = None):
        if buildId:
            return self.__buildsById.get(buildId, [])

        elif nodeId:
            return self.__lookupByNode(nodeId)

        elif clientId:
            return self.__lookupByClient(clientId)

        elif status:
            return self.__lookupByStatus(status)
        
        return []

    def listBuilds(self, clientId = None, nodeId = None, status = None, limit = 10):
        if not clientId and not nodeId and not status:
            return self.__buildsById.values()[-10:]

        byClient = []
        byNode = []
        byStatus = []
        filters = []
        if clientId:
            byClient = self.getBuild(clientId = clientId)
            filters.append(byClient)
        
        if nodeId:
            byNode = self.getBuild(nodeId = nodeId)
            filters.append(byNode)

        if status:
            byStatus = self.getBuild(status = status)
            filters.append(byStatus)

        #append lists, and remove dupes
        uniqueBuilds = set(byClient + byNode + byStatus)
        builds = list(uniqueBuilds.intersection(*filters))

        return builds[len(builds)-limit:]


    def __lookupByNode(self, nodeId):
        builds = []
        buildIds = self.__buildsByNode.get(nodeId, [])
        for Id in buildIds:
            builds.append(self.__buildsById[Id])
        return builds

    def __lookupByClient(self, clientId):
        builds = []
        buildIds = self.__buildsByClient.get(clientId,[])
        for Id in buildIds:
            builds.append(self.__buildsById[Id])
        return builds

    def __lookupByStatus(self, status):
        builds = []
        for buildId, build in self.__buildsById.iteritems():
            if build.status == status:
                builds.append(build)
        return builds

    def removeBuild(self, build):
        if build in self.__buildsById.values():
            try:
                self.__buildsByClient[build.clientID].remove(build.id)

            except ValueError:
                self.log().exception('Build-%s ID not found in clientDict'%build.id)
            
            except KeyError:
                self.log().exception('Build-%s ClientId not found in buildsByClient - ClientId %s'%(build.id, build.clientID))
            
            try:
                self.__buildsByNode[build.nodeId].remove(build.id)
            except ValueError:
                self.log().exception('Build-%s ID not found in nodeDict'%build.id)
            
            except KeyError:
                self.log().exception('Build-%s NodeId not found in buildsByNode - NodeId %s'%(build.id, build.nodeId))
                self.log(str(self.__buildsByNode))

            self.__buildsById.pop(build.id, None)

            return True, None

        else:
            self.log().error('Build-%s Not Found'%build.id)
            return False, 'Build not Found'
