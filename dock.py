import os
from docker import Client
from docker import errors as dockErrors
import logging
import uuid
from requests import exceptions as requErrors
import threading
import time
from utils import Status, Log

class dockApi(Log):
    def __init__(self):
        super(dockApi, self).__init__()
        self.__connected = threading.Event()
        self.connectLock = threading.Lock()
        self.createLock = threading.Lock()
        self.startLock = threading.Lock()
        self.dockerClient = None

    def connect(self):
        if not self.isConnected():
            with self.connectLock:
                self.__connect()

    def __connect(self):    
        try:
            self.log('Node-%s connecting to %s - API_VERSION %s'%(self.id, self.clientArgs['base_url'], self.clientArgs['version']))
            if not self.dockerClient:
                self.dockerClient = Client(**self.clientArgs)
            self.dockerClient.ping()
    
        except requErrors.ConnectionError as err:
            self.log().error('Node-%s FAILED to connect with: %s'%(self.id, err))
        except dockErrors.APIError as err:
            self.log().error('Node-%s FAILED to connect with: %s'%(self.id, err.reason))

        else:
            self.__connected.set()
        
            self.log().info('Node-%s connected to %s successfully!'%(self.id,self.clientArgs['base_url']))
            self.dockerInfo = self.dockerClient.version()
    
    def create_container(self, build, callback=None):
        if not self.isConnected():
            self.log('Node-%s is NOT CONNECTED, waiting for connection'%self.id)
            self.connect()
            self.__connected.wait()
        with  self.createLock:
            if callback:
                callback(self.__create_container(build))
            else:
                return self.__create_container(build)

    def __create_container(self, build):
        if self.isConnected():
            try:
                self.log('Creating container for Build-%s, Image: %s, args: %s'%(build.id, build.image, str(build.dockerArgs)))
                container = self.dockerClient.create_container(build.image, **build.dockerArgs)
    
            except dockErrors.APIError as err:
                self.log().error('Node-%s FAILED to create container with: %s'%(self.id, err))
                return False, err    
                
            else:
                build.container = Container(container['Id'])
                self.log('Node-%s Successfully created Container-%s'%(self.id, build.container.id))
                build.setStatus(Status.CREATED)
                return True, None

        return False, 'Client is not Connected'

    def start_container(self, build, callback=None):
        if not self.isConnected():
            self.log('Node-%s is NOT CONNECTED, waiting for connection'%self.id)
            self.connect()
            self.__connected.wait()
        with self.startLock:
            if callback:
                callback(self.__start_container(build))
            else:
                return self.__start_container(build)

    def __start_container(self, build):
        if self.isConnected():
            container = build.container
            try:
                self.log('Starting Container-%s'%build.container.id)
                self.dockerClient.start(container.id)
    
            except dockErrors.APIError as err:
                self.log().error('Container-%s FAILED to start with: %s'%(container.id, err))
                container.setStatus(Status.ERROR)
                build.setStatus(Status.ERROR)
                return False, err
    
            else:
                self.log('Container-%s started successfully!'%build.container.id)
                container.setStatus(Status.RUNNING)
                build.setStatus(Status.RUNNING)
                return True, None
        
        return False, 'Client is not Connected'

    def remove_container(self, build):
        if self.isConnected():
            return self.__remove_container(build)

    def __remove_container(self, build):
        container = build.container
        try:
            self.log('Removing Container-%s'%container.id)
            self.dockerClient.remove_container(container.id, force=True)

        except dockErrors.APIError as err:
            self.log.exception('Container-%s FAILED to remove'%container.id)
            return False, err

        else:
            self.log('Container-%s removed successfully!'%(container.id))
            return True, None

            
    def isConnected(self):
        return self.__connected.isSet()


        
class Container(Status):
    def __init__(self, id):
        super(Container, self).__init__()
        self.timeCreated = time.time()
        self.id = id
        self.setStatus(Status.CREATED)
        
