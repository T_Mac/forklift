import yaml

def loadNodeConfig(file='nodes.yml'):
    with open(file, 'r') as nodeConfig:
        nodes = []
        for node in yaml.load_all(nodeConfig):
            nodes.append(node)
    return nodes

