from dock import dockApi

import uuid
import threading
from threading import Lock
import config
from utils import Status, Log

class Node(dockApi):
    def __init__(self, base_url='unix://var/run/docker.sock', maxBuilds=1, timeout=30, version='1.17'):
        super(Node, self).__init__()
        self.id = uuid.uuid4().hex
        self.log('Creating Node-%s with base_url %s, api_version %s'%(self.id, base_url, version))
        
        self.clientArgs = {}
        self.clientArgs['base_url'] = base_url
        self.clientArgs['version'] = version
        self.clientArgs['timeout'] = timeout

        self.maxBuilds = maxBuilds
        self.timeout = timeout
        self.dockerClient = None
        self.builds = 0
        threading.Thread(target=self.connect).start()
        
    def createBuild(self, build):
        self.log('Creating container for Build-%s'%build.id)
        status, err = self.create_container(build)
        if status:
            return True, None
        else:
            self.log().error('Creating container FAILED,for Build-%s'%build.id)
            return False, err

    def startBuild(self, build):
        self.log().info('Starting Container-%s for Build %s'%(build.container.id, build.id))
        build.setStatus(Status.STARTING)
        self.builds = self.builds + 1
        status = self.start_container(build) 
        if status:
            return True, None
        else:
            self.log().error('Starting container FAILED,for Build-%s'%build.id)
            return False, err

    def destroyBuild(self, build):
        self.log().info('Destroying Container-%s for Build-%s'%(build.container.id, build.id))
        self.builds = self.builds - 1
        status, err = self.remove_container(build)
        if status:
            return True, None
        else:
            self.log().error('Removing container FAILED, for Build-%s'%build.id)
            return False, err

    def getLoad(self):
       load = round(float(self.builds)/self.maxBuilds, 4)
       return load

    def __updateCallback(self, build):
        print(build.__dict__)
        if Status.FINISHED == build.status():
            self.dockerClient.remove_container(build.containerId, force=True)
            return True, None
        return False, 'Bad Status Code'


class NodeManager(Log):
    def __init__(self):
        super(NodeManager, self).__init__()
        self.nodes = {}
        self.buildLock = Lock()

    def connect(self):
        self.__connectToNodes()

    def createBuild(self, build):
        self.log('Recieved Build-%s, Deploying...'%build.id)
        
        node, err  = self.__selectNode()
          
        if err:
            self.log().error('Creating Build-%s - Error: %s'%(build.id, err))

            if err == 'NO_NODES_AVAILABLE':
                self.log().warning('All nodes are busy, cannot deploy build')
            return False, err

        if node:
            build.nodeId = node.id
            self.log('Creating Build-%s on Node-%s'%(build.id, node.id))
            status, err = node.createBuild(build)
            
            if err:
                self.log().error('Creating Build-%s - Error: %s'%(build.id, node.id))

                return False, err
        else:
            return False, 'No Nodes Available'

        return True, None
        
    

    def startBuild(self, build):        
        self.log('Recieved Build-%s, waiting for lock'%build.id)
        with self.buildLock:
            node, err = self.__selectNode()
            if node:
                build.nodeId = node.id
                self.log('Deploying build on %s with load: %s'%(node.id,str(self.nodes[node])))
                node.startBuild(build)
                self.__refreshLoads()
                return True, None

            else:
                self.log().error('NO nodes availiable for build, are there any connected?')
                return False, 'No nodes available'

    def removeBuild(self, build):
        self.log('Removing Build-%s'%build.id)
        node = self.getNodeById(build.nodeId)
        if node:
            status, err = node.destroyBuild(build)
            if status:
                return True, None
            else:
                return False, err

    def __selectNode(self):
        self.__refreshLoads()
        for node in sorted(self.nodes, key=self.nodes.get):
            if self.nodes[node] < 1:
                if node.isConnected():
                    return node, None


        return False, 'NO_NODES_AVAILABLE'

    def __refreshLoads(self):
        for node in self.nodes.keys():
            self.nodes[node] = node.getLoad()
    
    def __connectToNodes(self):
        self.log().info('Connecting to Nodes...')
        for nodeConfig in config.loadNodeConfig():
            base_url = nodeConfig['url']
            max_build = nodeConfig['maxBuilds']
            api_version = nodeConfig['apiVersion']

            n = Node(base_url=base_url, maxBuilds=max_build, version=api_version)
            self.log('Created Node-%s'%n.id)
            self.nodes[n] = 0

    def getNodeById(self, nodeId):
        for node in self.nodes.keys():
            if node.id in nodeId:
                return node

        return None
