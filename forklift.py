from flask import Flask, request, json
from build import Build, BuildManager
import logging

LOGFILE = 'forklift.log'                                        #Logfile
LOGLVL = logging.DEBUG                                          #Logging Level
LOGFORMAT = '%(asctime)s %(name)s %(levelname)s: %(message)s'   #Log message Format

logging.basicConfig(level=LOGLVL, filename=LOGFILE, format=LOGFORMAT, datefmt='%Y-%m-%d %H:%M:%S')
moduleLogger = logging.getLogger('Forklift')

app = Flask(__name__)
buildManager = BuildManager()

@app.route('/deploy',methods=['POST'])
def deploy():
    payload = request.get_json()
    try:
        build = Build(  client_id=payload['clientId'],
                    git_repo=payload['gitRepo'], 
                    image = payload['image'],
                    dockerArgs=payload['dockerArgs'],
                )
        moduleLogger.debug('got Build-%s, clientId: %s - gitRepo: %s - image: %s - dockerArgs: %s'%(build.id, build.clientID, build.git_repo, build.image, build.dockerArgs))
    except KeyError as err:
        logging.exception('Bad Request')
        return json.jsonify(error = 'Malformed Request'), 400
    else:
        status, err = buildManager.addBuild(build)
        if status:
            return json.jsonify(BuildId = build.id)
        elif err:
            return json.jsonify(error = 'All Nodes Busy'), 420
    return 'Error', 500

@app.route('/builds',methods=['GET'])
def getBuilds():
    limit = int(request.args.get('limit', 10))
    status = request.args.get('status', None)
    nodeId = request.args.get('nodeId', None)
    clientId = request.args.get('clientId', None)
    builds = buildManager.listBuilds(clientId = clientId, nodeId = nodeId, status = status, limit = limit)
    buildDict = {}
    for build in builds:
        buildDict[build.id] = build.toDict()

    if builds == False:
        return json.jsonify(error = 'Bad status code'), 400

    return json.jsonify(buildDict)

@app.route('/update/<buildId>', methods=['POST'])
def updateBuild(buildId):
    payload = request.get_json()
    status = int(payload.get('status', None))
    error = payload.get('error', None)
    status, error = buildManager.updateBuild(buildId = buildId, **payload)

    returnCode = 200
    
    if error:
        returnCode = 503

    return json.jsonify(status=status, error=error), returnCode


if __name__ == '__main__':
    #Enable Debug Mode
    app.debug = True
    app.run(host='0.0.0.0')
